import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { FoodcourtSummaryComponent } from './foodcourt-summary/foodcourt-summary.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { ContactusComponent } from './contactus/contactus.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { StoreSummaryComponent } from './store-summary/store-summary.component';
import { DishSummaryComponent } from './dish-summary/dish-summary.component';
import { AccountComponent } from './account/account.component';


@NgModule({
  declarations: [
    AppComponent,
    FoodcourtSummaryComponent,
    WelcomeComponent,
    HeaderComponent,
    ContactusComponent,
    HoverHighlightDirective,
    StoreSummaryComponent,
    DishSummaryComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
