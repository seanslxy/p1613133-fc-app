import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { FoodcourtSummaryComponent } from './foodcourt-summary/foodcourt-summary.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { ContactusComponent } from './contactus/contactus.component';
import { StoreSummaryComponent } from './store-summary/store-summary.component';
import { DishSummaryComponent } from './dish-summary/dish-summary.component';
import { AccountComponent } from './account/account.component';



const appRoutes: Routes = [
  {path: '', redirectTo: '/home',pathMatch: 'full'},
  {path: 'home', component: WelcomeComponent},
  {path: 'summary', component: FoodcourtSummaryComponent},
  {path: 'contactus', component: ContactusComponent},
  {path: 'store', component: StoreSummaryComponent},
  {path: 'dish', component: DishSummaryComponent},
  {path: 'account', component: AccountComponent},
]


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],

  exports: [
    RouterModule,
      ],
    
  declarations: []
})
export class AppRoutingModule { }
