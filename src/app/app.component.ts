import { Component } from '@angular/core';
import { FoodCourtService } from "./shared/service/food-court.service";
import { StoreService } from "./shared/service/store.service";
import { DishSummaryService } from "./shared/service/dish.service";

@Component({
  
selector: 'app-root',
  
templateUrl: './app.component.html',
  
styleUrls: ['./app.component.css'],
  
providers: [FoodCourtService, DishSummaryService, StoreService]

})

export class AppComponent {
  
title = 'FoodCourts In Singapore Polytechnic';

}
