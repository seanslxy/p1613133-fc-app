import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactUsService } from '../shared/service/contactus.service';
import { ContactUs } from '../shared/model/contactus.model';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css'],
  providers: [ContactUsService]
})

export class ContactusComponent implements OnInit {
  Feedback: ContactUs[] = [];
  emailBlackList = ['test@test.com', 'temp@temp.com'];

  contactForm: FormGroup;
  submitted = false;

  constructor(private ContactUsService: ContactUsService) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      'userEmail': new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      'userFeedback': new FormControl(null, [Validators.required])
    });
  }

  blankSpaces(control: FormControl): { [s: string]: boolean } {
    if (control.value != null && control.value.trim().length === 0) {
      return { 'blankSpaces': true };
    }
    return null;
  }

  inEmailBlackList(control: FormControl): { [s: string]: boolean } {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return { 'emailBlackListed': true };
    }
    return null;
  }

  onSubmit() {
    console.log(this.contactForm);
    this.submitted = true;

    this.ContactUsService.createFeedback(this.contactForm["value"])
      .subscribe(data => this.Feedback = data)
  }
}