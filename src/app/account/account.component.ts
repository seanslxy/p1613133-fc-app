import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../shared/service/account.service';
import { Account } from '../shared/model/account.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [AccountService]
})

export class AccountComponent implements OnInit {
  Feedback: Account[] = [];
  emailBlackList = ['test@test.com', 'temp@temp.com'];

  contactForm: FormGroup;
  submitted = false;

  constructor(private AccountService: AccountService) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      'accountName': new FormControl(null, [Validators.required]),
      'accountEmail': new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      'accountPassword': new FormControl(null, [Validators.required])
    });
  }

  blankSpaces(control: FormControl): { [s: string]: boolean } {
    if (control.value != null && control.value.trim().length === 0) {
      return { 'blankSpaces': true };
    }
    return null;
  }

  inEmailBlackList(control: FormControl): { [s: string]: boolean } {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return { 'emailBlackListed': true };
    }
    return null;
  }

  onSubmit() {
    console.log(this.contactForm);
    this.submitted = true;

    this.AccountService.createFeedback(this.contactForm["value"])
      .subscribe(data => this.Feedback = data)
  }
}