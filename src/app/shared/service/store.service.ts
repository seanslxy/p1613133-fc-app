import { Injectable } from '@angular/core';
import { Store, Store1 } from"../model/store.model";


@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public StoreList = [
    new Store(1, "Japanese Store (Fc4)", "s_img01.jpg", "s_img04.jpg"),
    new Store(2, "Western Store (Fc4)", "s_img02.jpg", "s_img05.jpg"),
    new Store(3, "Korean Store (Fc4)", "s_img03.jpg", "s_img06.jpg"),
    new Store1(1, "Japanese store (Fc6)", "ss_img01.jpg", "ss_img04.jpg"),
    new Store1(2, "Chicken Rice Store (fc6)", "ss_img02.jpg", "ss_img05.jpg"),
    new Store1(3, "Salad Store (Fc6)", "ss_img03.jpg", "ss_img06.jpg"),
    ];
    
    constructor() { }
    getStore() {
    return this.StoreList.slice();
    }
    getStoreBysID(s_id) {
    for (let s of this.StoreList) {
    if (s.s_id == s_id) {
    return s;
    }
    }
    return null;
    }
    }
    
