import { Injectable } from '@angular/core';

import { Dish, Dish1, Dish2, Dish3, Dish4, Dish5 } from"../model/dish.model";

@Injectable()
export class DishSummaryService {

  public dishList = [
    
    new Dish(1, "Shoyu Ramen", "$4.00", "d_img01.jpg"),
    new Dish(2, "Sushi Platter", "$6.50", "d_img02.jpg"),
    new Dish(3, "Japanese Curry", "$4.00", "d_img03.jpg"),

    new Dish1(1, "Chicken Cutlet", "$4.00", "d_img04.jpg"),
    new Dish1(2, "Steak", "$6.50", "d_img05.jpg"),
    new Dish1(3, "Tomato Pasta", "$4.00", "d_img06.jpg"),

    new Dish2(1, "BBQ Chicken w Rice", "$4.00", "d_img07.jpg"),
    new Dish2(2, "BBQ Beef w Rice", "$5.00", "d_img08.jpg"),
    new Dish2(3, "Soba Fish w Rice", "$4.50", "d_img09.jpg"),

    new Dish3(1, "Tonkutsu Ramen", "$4.70", "dd_img01.jpg"),
    new Dish3(2, "OyakoDon", "$5.00", "dd_img02.jpg"),
    new Dish3(3, "KatsuDon", "$4.50", "dd_img03.jpg"),

    new Dish4(1, "Chicken Rice", "$3.00", "dd_img04.jpg"),
    new Dish4(2, "Brown Chicken Rice", "$3.00", "dd_img05.jpg"),
    new Dish4(3, "Fried Chicken Rice", "$3.50", "dd_img06.jpg"),

    new Dish5(1, "Chicken Salad", "$3.50", "dd_img07.jpg"),
    new Dish5(2, "Kale Salad", "$3.00", "dd_img08.jpg"),
    new Dish5(3, "Potato Salad", "$2.50", "dd_img09.jpg"),


  ];
  
  constructor() { }
  getDish() {
  return this.dishList.slice();
  }
  getDishBysID(d_id) {
  for (let d of this.dishList) {
  if (d.d_id == d_id) {
  return d;
  }
  }
  return null;
  }
  }
  