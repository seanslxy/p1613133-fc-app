import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()

export class AccountService {
    private ContactUs = [];

    constructor(private http: Http) { }

    createFeedback(fbDt) {
        return this.http.post("/api/account/", fbDt)
            .pipe(map((response: Response) => {
                const data = response.json();
                this.ContactUs = data;
                return data;
            },
                (error) => console.log(error)
            ));
    }
}