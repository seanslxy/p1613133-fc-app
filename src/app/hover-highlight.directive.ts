import { 
  Directive,
  ElementRef,
  Renderer2,
  OnInit,
  HostListener,
  HostBinding,
  Input
 } from '@angular/core';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

@HostBinding ('style.color') textColor: string;

  constructor(private elRef: ElementRef,
              private renderer: Renderer2) { }

@HostListener ('mouseenter') mouseOver(eventData: Event) {
this.renderer.setStyle(this.elRef.nativeElement, 'background-color', "green");
this.textColor = 'black';
}


@HostListener ('mouseleave') mouseExit(eventData: Event) {
  this.renderer.setStyle(this.elRef.nativeElement, 'background-color', "transparent");
  this.textColor = 'black';
  }

}
