import { Component, OnInit } from '@angular/core';
import { DishSummaryService } from "../shared/service/dish.service";
import { Dish, Dish1, Dish2, Dish3, Dish4, Dish5 } from "../shared/model/dish.model";

@Component({
  selector: 'app-dish-summary',
  templateUrl: './dish-summary.component.html',
  styleUrls: ['./dish-summary.component.css']
})

export class DishSummaryComponent implements OnInit {

  dishArr : Dish [] = [];
  dishArr1 : Dish1 [] = [];
  dishArr2 : Dish2 [] = [];
  dishArr3 : Dish3 [] = [];
  dishArr4 : Dish4 [] = [];
  dishArr5 : Dish5 [] = [];

  constructor(public dishService : DishSummaryService) { }

  ngOnInit() {
    this.dishArr = this.dishService.getDish();
  }

}