import { Component, OnInit } from '@angular/core';
import { StoreService } from "../shared/service/store.service";
import { Store, Store1 } from "../shared/model/store.model";


@Component({
  selector: 'app-store-summary',
  templateUrl: './store-summary.component.html',
  styleUrls: ['./store-summary.component.css']
})
export class StoreSummaryComponent implements OnInit {
  
  storeArr : Store [] = [];
  StoreArr1: Store1 [] = [];

  constructor(public storeService : StoreService) { }

  ngOnInit() {
    this.storeArr = this.storeService.getStore();
    
  }

}
